//
//  ViewController.swift
//  HecateDemo
//
//  Created by Rafael Costa on 11/9/15.
//  Copyright © 2015 Rafael Costa. All rights reserved.
//

import UIKit
import AFNetworking

class ViewController: UIViewController {
	let sessionManager = AFURLSessionManager(sessionConfiguration: NSURLSessionConfiguration.defaultSessionConfiguration())
	let securityPolicy = AFSecurityPolicy(pinningMode: AFSSLPinningMode.CA)
	
	@IBAction func signIn() {
		
		let serializer = AFHTTPRequestSerializer()
		let request = serializer.requestWithMethod("POST", URLString: "https://api.rafaelcosta.me:8079/auth/requestToken", parameters: [
			"application_id" : applicationId,
			"application_secret" : applicationSecret], error: nil)
		
		let certificatePath = NSBundle.mainBundle().pathForResource("ca_cert", ofType: "pem")
		
		securityPolicy.pinnedCertificates = [CertificateHelper.certificateDataFromFile(certificatePath!)]
		
		sessionManager.securityPolicy = securityPolicy
		
		sessionManager.responseSerializer = AFJSONResponseSerializer()
		
		let task = sessionManager.dataTaskWithRequest(request) { (response: NSURLResponse, returnData: AnyObject?, error: NSError?) -> Void in
			if error == nil {
				let json = returnData as! [String : AnyObject]
				print("Request Token: \(json["Request Token"]!)")
			} else {
				print("Error: \(error.debugDescription)")
			}
		}
		
		task.resume()
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}


}

