//
//  CertificateHelper.swift
//  HecateDemo
//
//  Created by Rafael Costa on 11/9/15.
//  Copyright © 2015 Rafael Costa. All rights reserved.
//

import UIKit

class CertificateHelper {
	class func certificateDataFromFile(file: String!) -> NSData {
		let certificatePath = file
		var certificateString = try! NSString(contentsOfFile: certificatePath, encoding: NSUTF8StringEncoding)
		certificateString = certificateString.stringByReplacingOccurrencesOfString("-----BEGIN CERTIFICATE-----", withString: "")
		certificateString = certificateString.stringByReplacingOccurrencesOfString("-----END CERTIFICATE-----", withString: "")
		certificateString = certificateString.stringByReplacingOccurrencesOfString("\r\n", withString: "")
		certificateString = certificateString.stringByReplacingOccurrencesOfString("\n", withString: "")
		
		let data = NSData(base64EncodedString: (certificateString as String), options: NSDataBase64DecodingOptions.IgnoreUnknownCharacters)
		
		return data!
	}
}
